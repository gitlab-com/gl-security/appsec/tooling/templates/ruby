# Project Name

This is a template project. Clone this project, and replace the README
with your own.

Configure projects settings as shown in the Project Settings section.

## Usage

```sh
bundle install
```

It comes with the following:

- Baseline rubocop rules
- Basic rspec set up (no tests)
- A `lib` folder for most of your code
- A `spec` folder for your tests
- Some gems:
  - The gitlab gem for making API calls easier: https://github.com/NARKOZ/gitlab
  - The require_all gem for requiring files easier: https://github.com/jarmo/require_all

You might also want to add a `bin` folder for executables.

## Tests

```sh
bundle exec rspec
```

With the `'rspec-parameterized'` gem you can write tests like:

```ruby
# Table Syntax Style (like Groovy spock)
# Need ruby-2.1 or later
describe "plus" do
  using RSpec::Parameterized::TableSyntax

  where(:a, :b, :answer) do
    1 | 2 | 3
    5 | 8 | 13
    0 | 0 | 0
  end

  with_them do
    it "should do additions" do
      expect(a + b).to eq answer
    end
  end
end
```

## Project Defaults

(Optional) Copy-paste this into an issue for tracking:

1. [ ] Configure default settings
    1. Navigate to `Settings > Repository`
        1. [ ] Select `Select push rules > Reject unverified users`, `Reject unsigned commits`, and `Prevent pushing secret files`
    1. Navigate to `Settings > Merge Requests`
        1. [ ] Select `Merge method > Fast-forward merge`
        1. [ ] Select all checkboxes under `Merge options`
        1. [ ] Select `Squash commits when merging > Encourage`
        1. [ ] Select `Merge checks > Pipelines must succeed`, `All threads must be resolved`, and `Status checks must succeed`
    1. (If planning to use Scheduled Pipelines) Navigate to `Settings > Integrations`
        1. Select `Configure` next to `Pipeline status emails`
        1. [ ] Select `Enable integration > Active`
        1. In Slack right-click `#sec-product-security-engineering`, then `Integrations` then `Send emails to ...`. Copy the email address.
        1. [ ] Set the recipient to the email address
        1. [ ] Select `Notify only broken pipelines`
1. [ ] Add the project to our [internal handbook](https://internal.gitlab.com/handbook/security/product_security/product_security_engineering/#project-inventory)
